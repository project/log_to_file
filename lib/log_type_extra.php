<?PHP


	/****************************************************************
	 * Class: obj_log_type_extra
	 * Date: 2008-02-10
	 * The purpose of this class is to extend the base class for
	 * user defined functions, properties and methods.
	 ***************************************************************/


	include_once('log_type.php');


	Class obj_log_type_extra extends obj_log_type {
	
		/*****************************************************
		* Begin find_by_type
		****************************************************/
		// Function finds item from log_type based on the supplied type
		public function find_by_type() {
	
			$sql = 'SELECT log_type_id';
			$sql .= ', log_type';
			$sql .= ', log_type_description';
			$sql .= ' FROM ' . $this->table_name;
			$sql .= ' WHERE log_type = ' . log_quote($this->log_type);
			$result = db_query($sql);
	
			if( $row = db_fetch_object($result) ) {
				$this->fill_object($row);
				return true;
			} else {
				return false;
			}
		}
		/*****************************************************
		* End find_by_type
		****************************************************/
	
	};
?>