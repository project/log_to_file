<?PHP

include_once('data_helper.php');

/**********************************************************************
 * Class: objlog_level
 * Author: Code Generator
 * Date: 2008-02-10
 *********************************************************************/

Class obj_log_level {

	var $table_name = 'log_level';

	var $log_level_id;
	var $log_level;
	var $log_level_long_name;
	var $log_level_short_name;
	var $log_level_description;

	/*****************************************************
	 * Begin constructor
	 ****************************************************/
	// Constructor
	public function __construct() {

		// Initialize class properties
		// useful for when update or insert is called
		// and all values are not set
		$this->log_level_id = 0;
		$this->log_level = 0;
		$this->log_level_long_name = '';
		$this->log_level_short_name = '';
		$this->log_level_description = '';
	}
	/*****************************************************
	 * End constructor
	 ****************************************************/

	/*****************************************************
	 * Begin fill
	 ****************************************************/
	// Populates a class object with values from another class or an array
	protected function fill_object($obj_data) {

		if( gettype($obj_data) == 'object' ) {

			// Object is another instance of class, assign values accordingly
			$this->log_level_id = $obj_data->log_level_id;
			$this->log_level = $obj_data->log_level;
			$this->log_level_long_name = $obj_data->log_level_long_name;
			$this->log_level_short_name = $obj_data->log_level_short_name;
			$this->log_level_description = $obj_data->log_level_description;
		} else {
			$this->log_level_id = $obj_data['log_level_id'];
			$this->log_level = $obj_data['log_level'];
			$this->log_level_long_name = $obj_data['log_level_long_name'];
			$this->log_level_short_name = $obj_data['log_level_short_name'];
			$this->log_level_description = $obj_data['log_level_description'];
		}
	}
	/*****************************************************
	 * End fill
	 ****************************************************/

	/*****************************************************
	 * Begin find
	 ****************************************************/
	// Function finds item from log_level based on the primary key
	public function find() {

		$sql = 'SELECT log_level_id';
		$sql .= ', log_level';
		$sql .= ', log_level_long_name';
		$sql .= ', log_level_short_name';
		$sql .= ', log_level_description';
		$sql .= ' FROM ' . $this->table_name;
		$sql .= ' WHERE log_level_id = ' . $this->log_level_id;
		$result = db_query($sql);

		if( $row = db_fetch_object($result) ) {
			$this->fill_object($row);
			return true;
		} else {
			return false;
		}
	}
	/*****************************************************
	 * End find
	 ****************************************************/

	/*****************************************************
	 * Begin find_many
	 ****************************************************/
	// Finds all records in table or records mathcing passed where clause
	public function find_many($sql_ext = '', $order_ext = '') {

		$sql = 'SELECT log_level_id';
		$sql .= ', log_level';
		$sql .= ', log_level_long_name';
		$sql .= ', log_level_short_name';
		$sql .= ', log_level_description';
		$sql .= ' FROM ' . $this->table_name;

		if( $sql_ext != '' ) {
			$sql .= ' WHERE ' . $sql_ext;
		}
		if( $order_ext != '' ) {
			$sql .= ' ' . $order_ext;
		}
		$result = db_query($sql);
		$count = 0;

		while( $row = db_fetch_object($result) ) {
			$item = new obj_log_level();
			$item->fill_object($row);
			$return_data[$count++] = $item;
		}
		return $return_data;
	}
	/*****************************************************
	 * End find_many
	 ****************************************************/

	/*****************************************************
	 * Begin find_many_paginated
	 ****************************************************/
	// Finds all records in table or records mathcing passed where clause
	public function find_many_paginated($sorted_header, $sql_ext = '', $order_ext = '') {

		$sql = 'SELECT log_level_id';
		$sql .= ', log_level';
		$sql .= ', log_level_long_name';
		$sql .= ', log_level_short_name';
		$sql .= ', log_level_description';
		$sql .= ' FROM ' . $this->table_name;

		if( $sql_ext != '' ) {
			$sql .= ' WHERE ' . $sql_ext;
		}
		if( $order_ext != '' ) {
			$sql .= ' ' . $order_ext;
		}
		$result_count = '
			SELECT
				count(log_level_id) as this_count
			FROM
			(
				' . $sql . '
			) count_table
		';
		$result = pager_query
		(
			db_rewrite_sql
			('
				' . $sql . '
				' . $sorted_header
				, 'log_level'
				, 'log_level_id'
			),
			variable_get('log_level_paginate', 20),
			0,
			$result_count
		);
		return $result;
	}
	/*****************************************************
	 * End find_many_paginated
	 ****************************************************/

	/*****************************************************
	 * Begin get_num_rows
	 ****************************************************/
	// This function will return the number of rows/entries in a table
	public function get_num_rows() {

		$sql = 'SELECT COUNT(log_level_id) as total_entries FROM ' . $this->table_name;
		$num_rows = db_fetch_object(db_query($sql));
		return $num_rows->total_entries;
	}
	/*****************************************************
	 * End get_num_rows
	 ****************************************************/

	/*****************************************************
	 * Begin update
	 ****************************************************/
	// Updates database with object properties if primary key is set or creates new entry if no primary key
	public function update() {

		if( $this->log_level_id == 0 ) {
			$sql = 'INSERT INTO ' . $this->table_name . ' ( ';
			$sql .= 'log_level';
			$sql .= ', log_level_long_name';
			$sql .= ', log_level_short_name';
			$sql .= ', log_level_description';
			$sql .= ') VALUES (';
			$sql .= '' . $this->log_level;
			$sql .= ', ' . log_quote($this->log_level_long_name);
			$sql .= ', ' . log_quote($this->log_level_short_name);
			$sql .= ', ' . log_quote($this->log_level_description);
			$sql .= ')';
		} else {
			$sql = 'UPDATE ' . $this->table_name;
			$sql .= ' SET ';
			$sql .= 'log_level = ' . $this->log_level;
			$sql .= ', log_level_long_name = ' . log_quote($this->log_level_long_name, true);
			$sql .= ', log_level_short_name = ' . log_quote($this->log_level_short_name, true);
			$sql .= ', log_level_description = ' . log_quote($this->log_level_description, true);
			$sql .= ' WHERE log_level_id = ' . $this->log_level_id;
		}
		db_query($sql);

		if( $this->log_level_id == 0 ) {
			$result = db_query('SELECT @@IDENTITY ident FROM ' . $this->table_name);
			$data = db_fetch_object($result);
			$this->log_level_id = $data->ident;
			$this->find();
		}

	}
	/*****************************************************
	 * End update
	 ****************************************************/

	/*****************************************************
	 * Begin delete
	 ****************************************************/
	// This function will delete the record from table specified by primary key
	public function delete() {

		$sql = 'DELETE FROM ' . $this->table_name . ' WHERE log_level_id = ' . $this->log_level_id;
		db_query($sql);
	}
	/*****************************************************
	 * End delete
	 ****************************************************/

	/*****************************************************
	 * Begin destructor
	 ****************************************************/
	public function __destruct() {
		unset($this->log_level_id, $this->log_level, $this->log_level_long_name, $this->log_level_short_name, $this->log_level_description);
	}
	/*****************************************************
	 * End destructor
	 ****************************************************/

}