<?PHP

	function log_quote($value, $force = false) {
		// Stripslashes
		if (get_magic_quotes_gpc()) {
			$value = stripslashes($value);
		}
		// Quote if not integer
		if (!is_numeric($value)) {
			$value = "'" . mysql_escape_string($value) . "'";
		}
		return $value;
	}

?>