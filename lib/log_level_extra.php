<?PHP


	/****************************************************************
	 * Class: obj_log_level_extra
	 * Date: 2008-02-10
	 * The purpose of this class is to extend the base class for
	 * user defined functions, properties and methods.
	 ***************************************************************/


	include_once('log_level.php');


	Class obj_log_level_extra extends obj_log_level {

	/*****************************************************
	 * Begin find_by_level
	 ****************************************************/
	// Function finds item from log_level based on the log level
	public function find_by_long_name() {

		$sql = 'SELECT log_level_id';
		$sql .= ', log_level';
		$sql .= ', log_level_long_name';
		$sql .= ', log_level_short_name';
		$sql .= ', log_level_description';
		$sql .= ' FROM ' . $this->table_name;
		$sql .= ' WHERE log_level_long_name = ' . log_quote($this->log_level_long_name);
		$result = db_query($sql);

		if( $row = db_fetch_object($result) ) {
			$this->fill_object($row);
			return true;
		} else {
			return false;
		}
	}
	/*****************************************************
	 * End find_by_level
	 ****************************************************/

	};
?>