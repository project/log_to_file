<?php


/**********************************************************************
 * Name: log_to_file_admin_extra.inc
 * Author: Code Generator
 * Date: 2008-02-10
 * 
 * Description: Extra Administrative functions for log_to_file
 *              Drupal Module.
 *********************************************************************/


/**
 * Additional Admin functions for the Drupal Log To File
 * Module should be added to this file, in case you wish to re-generate
 * these files again later.
 */

/**
 * Drupal Log To File Administrative Settings Page
 */
function log_to_file_admin_extra_settings() {
	$items = array();

	/**
	 * Here is an example of a setting that will allow you to choose a specific
	 * node type to use for your node module.

	$items['handicapping_nodetype'] = array(
		'#type'          => 'radios',
		'#title'         => t('Node Type'),
		'#options'       => node_get_types('names'),
		'#default_value' => variable_get('log_to_file_nodetype', 'story'),
		'#description'   => t('What node type are these articles stored under?'),
	);

	*/
	return $items;
}

