<?php


/**********************************************************************
 * Name: log_to_file_admin.inc
 * Author: Code Generator
 * Date: 2008-02-10
 * 
 * Description: Administrative functions for log_to_file
 *              Drupal Module.
 *********************************************************************/


/**
 * log_to_file_admin_main()
 *
 * This is the main page for all CRUD (Create, Retrieve, Update and Delete)
 * tasks associated with DB tables/classes for this module.
 */
function log_to_file_admin_main() {
	$log_level = new obj_log_level_extra();
	$log_type = new obj_log_type_extra();
	$output = '';
	$header = array(
		array('data' => t('<b>Table Name</b>')),
		array('data' => t('<b>No Entries</b>')),
	);
	$rows[] = array(
		array('data' => l(t('log_level'), 'admin/log_to_file/log_level')),
		array('data' => $log_level->get_num_rows()),
	);
	$rows[] = array(
		array('data' => l(t('log_type'), 'admin/log_to_file/log_type')),
		array('data' => $log_type->get_num_rows()),
	);
	$output .= theme('table', $header, $rows);
	return $output;
}

/**
 * log_to_file_admin_add_log_level()
 *
 * Form hook
 *
 * This function will present the user with a form to add a new entry
 * to the log_level table in the database. 
 */
function log_to_file_admin_add_log_level() {
	$log_level = new obj_log_level_extra();
	$log_level->log_level_id = 0;
	$form['log_level_id'] = array(
		'#type' => 'textfield',
		'#size' => 80,
		'#maxlength' => 255,
		'#default_value' => '0',
		'#title' => t('log_level_id'),
		'#disabled' => TRUE,
	);

	$form['log_level'] = array(
		'#type' => 'textfield',
		'#size' => 80,
		'#maxlength' => 255,
		'#title' => t('log_level'),
		'#required' => TRUE,
	);
	$form['log_level_long_name'] = array(
		'#type' => 'textfield',
		'#size' => 80,
		'#maxlength' => 255,
		'#title' => t('log_level_long_name'),
		'#required' => TRUE,
	);
	$form['log_level_short_name'] = array(
		'#type' => 'textfield',
		'#size' => 80,
		'#maxlength' => 255,
		'#title' => t('log_level_short_name'),
		'#required' => TRUE,
	);
	$form['log_level_description'] = array(
		'#type' => 'textfield',
		'#size' => 80,
		'#maxlength' => 255,
		'#title' => t('log_level_description'),
	);
	$form['log_level_submit'] = array(
		'#type' => 'submit',
		'#value' => t('Submit'),
	);
	return $form;
}

/**
 * log_to_file_admin_add_log_level_submit()
 *
 * Form _submit hook
 *
 * This function will go through all fields in log_level, setting
 * each to the user-supplied values.  The primary key is set to 0
 * so that the class is aware that this is a new entry.
 */
function log_to_file_admin_add_log_level_submit($form_id, $form_values) {
	$log_level = new obj_log_level_extra();
	$log_level->log_level_id = 0;
	$log_level->find();
	$log_level->log_level = $form_values['log_level'];
	$log_level->log_level_long_name = $form_values['log_level_long_name'];
	$log_level->log_level_short_name = $form_values['log_level_short_name'];
	$log_level->log_level_description = $form_values['log_level_description'];
	$log_level->update();
	drupal_goto('admin/log_to_file/log_level');
}

/**
 * log_to_file_admin_delete_log_level()
 *
 * Form hook
 *
 * This function will find the id from the log_level class and display
 * an uneditable form with existing data, allowing the user to confirm 
 * that they wish to delete this entry.
 */
function log_to_file_admin_delete_log_level($id = 0) {
	$log_level = new obj_log_level_extra();
	$log_level->log_level_id = $id;
	$log_level->find();

	$form['log_level_id'] = array(
		'#type' => 'textfield',
		'#size' => 80,
		'#maxlength' => 255,
		'#default_value' => $log_level->log_level_id,
		'#title' => t('log_level_id'),
		'#disabled' => TRUE,
	);

	$form['log_level'] = array(
		'#type' => 'textfield',
		'#size' => 80,
		'#maxlength' => 255,
		'#default_value' => $log_level->log_level,
		'#title' => t('log_level'),
		'#disabled' => TRUE,
	);

	$form['log_level_long_name'] = array(
		'#type' => 'textfield',
		'#size' => 80,
		'#maxlength' => 255,
		'#default_value' => $log_level->log_level_long_name,
		'#title' => t('log_level_long_name'),
		'#disabled' => TRUE,
	);

	$form['log_level_short_name'] = array(
		'#type' => 'textfield',
		'#size' => 80,
		'#maxlength' => 255,
		'#default_value' => $log_level->log_level_short_name,
		'#title' => t('log_level_short_name'),
		'#disabled' => TRUE,
	);

	$form['log_level_description'] = array(
		'#type' => 'textfield',
		'#size' => 80,
		'#maxlength' => 255,
		'#default_value' => $log_level->log_level_description,
		'#title' => t('log_level_description'),
		'#disabled' => TRUE,
	);

	$form['log_level_submit'] = array(
		'#type' => 'submit',
		'#value' => t('Confirm Delete'),
	);
	$form['log_level_cancel'] = array(
		'#value' => l(t('Cancel'), 'admin/log_to_file/log_level'),
	);
	return $form;
}

/**
 * log_to_file_admin_delete_log_level_submit()
 *
 * Form _submit hook
 *
 * This function will find the id from the log_level class and delete
 * the corresponding entry.  Then the user will be sent back to the view
 * function.
 */
function log_to_file_admin_delete_log_level_submit($form_id, $form_values) {
	$log_level = new obj_log_level_extra();
	$log_level->log_level_id = $form_values['log_level_id'];
	$log_level->find();
	$log_level->delete();
	drupal_goto('admin/log_to_file/log_level');
}

/**
 * log_to_file_admin_edit_log_level()
 *
 * Form hook
 *
 * This function will find the id from the log_level class and display
 * a form with existing data, allowing the user to change all but the 
 * primary key.
 */
function log_to_file_admin_edit_log_level($id = 0) {
	$log_level = new obj_log_level_extra();
	$log_level->log_level_id = $id;
	$log_level->find();

	$form['log_level_id'] = array(
		'#type' => 'textfield',
		'#size' => 80,
		'#maxlength' => 255,
		'#default_value' => $log_level->log_level_id,
		'#title' => t('log_level_id'),
		'#disabled' => TRUE,
	);

	$form['log_level'] = array(
		'#type' => 'textfield',
		'#size' => 80,
		'#maxlength' => 255,
		'#default_value' => $log_level->log_level,
		'#title' => t('log_level'),
		'#required' => TRUE,
	);
	$form['log_level_long_name'] = array(
		'#type' => 'textfield',
		'#size' => 80,
		'#maxlength' => 255,
		'#default_value' => $log_level->log_level_long_name,
		'#title' => t('log_level_long_name'),
		'#required' => TRUE,
	);
	$form['log_level_short_name'] = array(
		'#type' => 'textfield',
		'#size' => 80,
		'#maxlength' => 255,
		'#default_value' => $log_level->log_level_short_name,
		'#title' => t('log_level_short_name'),
		'#required' => TRUE,
	);
	$form['log_level_description'] = array(
		'#type' => 'textfield',
		'#size' => 80,
		'#maxlength' => 255,
		'#default_value' => $log_level->log_level_description,
		'#title' => t('log_level_description'),
	);
	$form['log_level_submit'] = array(
		'#type' => 'submit',
		'#value' => t('Submit'),
	);
	return $form;
}

/**
 * log_to_file_admin_edit_log_level_submit()
 *
 * Form _submit hook
 *
 * This function will find the id from the log_level class and update
 * the corresponding entry with supplied data.  Then the user will be 
 * sent back to the view function.
 */
function log_to_file_admin_edit_log_level_submit($form_id, $form_values) {
	$log_level = new obj_log_level_extra();
	$log_level->log_level_id = $form_values['log_level_id'];
	$log_level->find();
	$log_level->log_level = $form_values['log_level'];
	$log_level->log_level_long_name = $form_values['log_level_long_name'];
	$log_level->log_level_short_name = $form_values['log_level_short_name'];
	$log_level->log_level_description = $form_values['log_level_description'];
	$log_level->update();
	drupal_goto('admin/log_to_file/log_level');
}

/**
 * log_to_file_admin_view_log_level()
 *
 *
 */
function log_to_file_admin_view_log_level() {
	$log_level = new obj_log_level_extra();
	$output = '';
	$output .= t('<b>log_level Data</b>');
	$output .= '<br /><br />';
	$header = array(
		array('data' => t('<b>log_level_id</b>'), 'field' => 'log_level_id'),
		array('data' => t('<b>log_level</b>'), 'field' => 'log_level'),
		array('data' => t('<b>log_level_long_name</b>'), 'field' => 'log_level_long_name'),
		array('data' => t('<b>log_level_short_name</b>'), 'field' => 'log_level_short_name'),
		array('data' => t('<b>log_level_description</b>'), 'field' => 'log_level_description'),
		array('data' => t('<b>Options</b>')),
	);
	$sql = tablesort_sql($header);
	$table_data = $log_level->find_many_paginated($sql);
	if(db_num_rows($table_data) >= 1) {
		while($row_data = db_fetch_object($table_data)){
			$rows[] = array(
				array('data' => $row_data->log_level_id),
				array('data' => $row_data->log_level),
				array('data' => $row_data->log_level_long_name),
				array('data' => $row_data->log_level_short_name),
				array('data' => $row_data->log_level_description),
				array('data' => l(t(Edit), 'admin/log_to_file/log_level/edit/' . $row_data->log_level_id) . ' | ' . l(t(Delete), 'admin/log_to_file/log_level/delete/' . $row_data->log_level_id)),
			);
		}
	}
	$output .= theme('table', $header, $rows);
	$output .= theme('pager', NULL, variable_get('log_level_paginate', 20));
	$output .= '<br />';
	return $output;
}

/**
 * log_to_file_admin_add_log_type()
 *
 * Form hook
 *
 * This function will present the user with a form to add a new entry
 * to the log_type table in the database. 
 */
function log_to_file_admin_add_log_type() {
	$log_type = new obj_log_type_extra();
	$log_type->log_type_id = 0;
	$form['log_type_id'] = array(
		'#type' => 'textfield',
		'#size' => 80,
		'#maxlength' => 255,
		'#default_value' => '0',
		'#title' => t('log_type_id'),
		'#disabled' => TRUE,
	);

	$form['log_type'] = array(
		'#type' => 'textfield',
		'#size' => 80,
		'#maxlength' => 255,
		'#title' => t('log_type'),
	);
	$form['log_type_description'] = array(
		'#type' => 'textfield',
		'#size' => 80,
		'#maxlength' => 255,
		'#title' => t('log_type_description'),
	);
	$form['log_type_submit'] = array(
		'#type' => 'submit',
		'#value' => t('Submit'),
	);
	return $form;
}

/**
 * log_to_file_admin_add_log_type_submit()
 *
 * Form _submit hook
 *
 * This function will go through all fields in log_type, setting
 * each to the user-supplied values.  The primary key is set to 0
 * so that the class is aware that this is a new entry.
 */
function log_to_file_admin_add_log_type_submit($form_id, $form_values) {
	$log_type = new obj_log_type_extra();
	$log_type->log_type_id = 0;
	$log_type->find();
	$log_type->log_type = $form_values['log_type'];
	$log_type->log_type_description = $form_values['log_type_description'];
	$log_type->update();
	drupal_goto('admin/log_to_file/log_type');
}

/**
 * log_to_file_admin_delete_log_type()
 *
 * Form hook
 *
 * This function will find the id from the log_type class and display
 * an uneditable form with existing data, allowing the user to confirm 
 * that they wish to delete this entry.
 */
function log_to_file_admin_delete_log_type($id = 0) {
	$log_type = new obj_log_type_extra();
	$log_type->log_type_id = $id;
	$log_type->find();

	$form['log_type_id'] = array(
		'#type' => 'textfield',
		'#size' => 80,
		'#maxlength' => 255,
		'#default_value' => $log_type->log_type_id,
		'#title' => t('log_type_id'),
		'#disabled' => TRUE,
	);

	$form['log_type'] = array(
		'#type' => 'textfield',
		'#size' => 80,
		'#maxlength' => 255,
		'#default_value' => $log_type->log_type,
		'#title' => t('log_type'),
		'#disabled' => TRUE,
	);

	$form['log_type_description'] = array(
		'#type' => 'textfield',
		'#size' => 80,
		'#maxlength' => 255,
		'#default_value' => $log_type->log_type_description,
		'#title' => t('log_type_description'),
		'#disabled' => TRUE,
	);

	$form['log_type_submit'] = array(
		'#type' => 'submit',
		'#value' => t('Confirm Delete'),
	);
	$form['log_type_cancel'] = array(
		'#value' => l(t('Cancel'), 'admin/log_to_file/log_type'),
	);
	return $form;
}

/**
 * log_to_file_admin_delete_log_type_submit()
 *
 * Form _submit hook
 *
 * This function will find the id from the log_type class and delete
 * the corresponding entry.  Then the user will be sent back to the view
 * function.
 */
function log_to_file_admin_delete_log_type_submit($form_id, $form_values) {
	$log_type = new obj_log_type_extra();
	$log_type->log_type_id = $form_values['log_type_id'];
	$log_type->find();
	$log_type->delete();
	drupal_goto('admin/log_to_file/log_type');
}

/**
 * log_to_file_admin_edit_log_type()
 *
 * Form hook
 *
 * This function will find the id from the log_type class and display
 * a form with existing data, allowing the user to change all but the 
 * primary key.
 */
function log_to_file_admin_edit_log_type($id = 0) {
	$log_type = new obj_log_type_extra();
	$log_type->log_type_id = $id;
	$log_type->find();

	$form['log_type_id'] = array(
		'#type' => 'textfield',
		'#size' => 80,
		'#maxlength' => 255,
		'#default_value' => $log_type->log_type_id,
		'#title' => t('log_type_id'),
		'#disabled' => TRUE,
	);

	$form['log_type'] = array(
		'#type' => 'textfield',
		'#size' => 80,
		'#maxlength' => 255,
		'#default_value' => $log_type->log_type,
		'#title' => t('log_type'),
	);
	$form['log_type_description'] = array(
		'#type' => 'textfield',
		'#size' => 80,
		'#maxlength' => 255,
		'#default_value' => $log_type->log_type_description,
		'#title' => t('log_type_description'),
	);
	$form['log_type_submit'] = array(
		'#type' => 'submit',
		'#value' => t('Submit'),
	);
	return $form;
}

/**
 * log_to_file_admin_edit_log_type_submit()
 *
 * Form _submit hook
 *
 * This function will find the id from the log_type class and update
 * the corresponding entry with supplied data.  Then the user will be 
 * sent back to the view function.
 */
function log_to_file_admin_edit_log_type_submit($form_id, $form_values) {
	$log_type = new obj_log_type_extra();
	$log_type->log_type_id = $form_values['log_type_id'];
	$log_type->find();
	$log_type->log_type = $form_values['log_type'];
	$log_type->log_type_description = $form_values['log_type_description'];
	$log_type->update();
	drupal_goto('admin/log_to_file/log_type');
}

/**
 * log_to_file_admin_view_log_type()
 *
 *
 */
function log_to_file_admin_view_log_type() {
	$log_type = new obj_log_type_extra();
	$output = '';
	$output .= t('<b>log_type Data</b>');
	$output .= '<br /><br />';
	$header = array(
		array('data' => t('<b>log_type_id</b>'), 'field' => 'log_type_id'),
		array('data' => t('<b>log_type</b>'), 'field' => 'log_type'),
		array('data' => t('<b>log_type_description</b>'), 'field' => 'log_type_description'),
		array('data' => t('<b>Options</b>')),
	);
	$sql = tablesort_sql($header);
	$table_data = $log_type->find_many_paginated($sql);
	if(db_num_rows($table_data) >= 1) {
		while($row_data = db_fetch_object($table_data)){
			$rows[] = array(
				array('data' => $row_data->log_type_id),
				array('data' => $row_data->log_type),
				array('data' => $row_data->log_type_description),
				array('data' => l(t(Edit), 'admin/log_to_file/log_type/edit/' . $row_data->log_type_id) . ' | ' . l(t(Delete), 'admin/log_to_file/log_type/delete/' . $row_data->log_type_id)),
			);
		}
	}
	$output .= theme('table', $header, $rows);
	$output .= theme('pager', NULL, variable_get('log_type_paginate', 20));
	$output .= '<br />';
	return $output;
}

/**
 * Drupal Log To File Administrative Settings Page
 */
function log_to_file_admin_settings() {
	//Get any _extra_ settings
	$items = array_merge(log_to_file_admin_extra_settings());

	//Define Logging Options
	$log_levels = new obj_log_level_extra();
	$log_level = $log_levels->find_many();
	$logging_options = array();
	foreach($log_level as $ll => $data)
	{
		$logging_options[$data->log_level] = $data->log_level_short_name;
	}
	$logging_options[99] = t('Off');

	$default_log = array();
	$log_types = new obj_log_type_extra();
	$log_type = $log_types->find_many();
	foreach($log_type as $lt => $data)
	{
		$default_log[$data->log_type] = $data->log_type;
	}

	$items['log_to_file_directory'] = array(
		'#type'          => 'textfield',
		'#title'         => t('Path to Logging Directory'),
		'#default_value' => variable_get('log_to_file_directory', ''),
		'#size'          => 50,
		'#maxlength'     => 255,
		'#description'   => t('Which directory do you wish to store your logs (note, this directory must exist and be writeable by apache\'s user!'),
	);

	$items['log_to_file_name_num'] = array(
		'#type'          => 'radios',
		'#title'         => t('Log By Name or Number'),
		'#default_value' => variable_get('log_to_file_name_num', '0'),
		'#options'       => array(0 => 'By Log Number', 1 => 'By Log Short Name'),
		'#description'   => t('Would you like to write logs by log number (i.e. \'1\') or log name (i.e. \'ERROR\')?'),
	);

	$items['log_to_file_default_log'] = array(
		'#type'          => 'radios',
		'#title'         => t('Default Log'),
		'#default_value' => variable_get('log_to_file_default_log', '1'),
		'#options'       => $default_log,
		'#description'   => t('In the event that a logging error occurs, which log file should we log put the logging error into?'),
	);

	$log_types = new obj_log_type_extra();
	$log_type = $log_types->find_many();
	foreach($log_type as $lt => $data)
	{
		$items['log_to_file_' . strtolower($data->log_type) . '_filename'] = array(
			'#type'          => 'textfield',
			'#title'         => t($data->log_type . ' Log File Name'),
			'#default_value' => variable_get('log_to_file_' . strtolower($data->log_type) . '_filename', ''),
			'#size'          => 50,
			'#maxlength'     => 255,
			'#description'   => t('What is the name of the file you wish to log ' . $data->log_type . ' information to?'),
		);
	
		$items['log_to_file_' . strtolower($data->log_type) . '_minloglevel'] = array(
			'#type'          => 'radios',
			'#title'         => t($data->log_type . ' Minimum Logging Level'),
			'#default_value' => variable_get('log_to_file_' . strtolower($data->log_type) . '_minloglevel', 99),
			'#options'       => $logging_options,
			'#description'   => t('What is the minimum log level you wish to write to the ' . $data->log_type . ' log file?  Turning this to Debug or higher can be helpful when diagnosing an issue!'),
		);
	}

	$items['log_level_paginate'] = array(
		'#type'          => 'textfield',
		'#title'         => t('log_level Listings Per Page'),
		'#default_value' => variable_get('log_level_paginate', 20),
		'#size'          => 5,
		'#maxlength'     => 5,
		'#description'   => t('How many entries would you like per log_level admin page?'),
	);

	$items['log_type_paginate'] = array(
		'#type'          => 'textfield',
		'#title'         => t('log_type Listings Per Page'),
		'#default_value' => variable_get('log_type_paginate', 20),
		'#size'          => 5,
		'#maxlength'     => 5,
		'#description'   => t('How many entries would you like per log_type admin page?'),
	);

	return system_settings_form($items);
}

